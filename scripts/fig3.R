#!/usr/bin/Rscript
options(crayon.enabled=FALSE)
library(tidyverse)
library(patchwork)
library(arrow)
library(Rcpp) 

# To read all traces from the small sample 
## list.files(
##   "./data/traces_samplesize-6/", 
##   pattern="-[[:digit:]]\\.gz\\.parquet$", 
##   full.names=TRUE
##  ) %>% 
##  as_tibble() %>% 
##  rename(trace.file=value) %>% 
##  mutate(config=sub(".*/", "", trace.file)) %>% 
##  mutate(config=sub("\\.gz\\.parquet", "", config)) %>% 
##  mutate(trace=map(trace.file, read_parquet)) %>% 
##  select(-trace.file) -> traces 

read_parquet(
  "./data/traces_samplesize-6/1-1-1.gz.parquet"
) %>% 
  mutate(config="1-1-1") %>% 
  filter(Rank==2) %>%
  filter(End < 22 ) %>% 
  filter(
    grepl("em2dkx|em_primal|em_derivs|mpi_worker_run_subset|error_estimate|localRefinement", Operation)
  ) %>%
  select(-Imbrication) -> fig3.data

fig3.data %>% 
  mutate(Parameter=gsub("\\_", "", gsub("[A-Za-z]", "", Parameter))) %>%
  filter(Operation == "localRefinement") %>% 
  separate(
    col= Parameter,
    into=c("loop", "start", "btri", "atri", "stop", "nTx", "nRx"), 
    convert=TRUE
  ) %>% 
  mutate(next_=lead(loop, default=10000)) %>%
  select(Operation, Start, End, loop, next_) %>% 
  mutate(first=ifelse(loop == 1, TRUE, FALSE)) %>%
  mutate(last=ifelse(next_ == 1, TRUE, FALSE)) %>%
  filter(last==TRUE | first==TRUE) %>% 
  mutate(i=as.integer((1:n()-1)/2)) -> subs.tmp

subs.tmp %>% 
  mutate(Operation = "subset") %>% 
  group_by(Operation, i) %>% 
  summarize(Start=min(Start), End=max(End), .groups="keep") %>% 
  ungroup() -> fig3.subs

tibble(
  Operation=c("em2dkx", "subset", "em_primal", "em_derivs", "mpi_worker_run_subset", "error_estimate", "localRefinement"), 
  name=c("subset", "local_refinement", "em_primal", "em_derivs", "Grupo de Refinamento", "estimate_error", "localrefinement"), 
  Imbrication = c(1.5, 2, 2, 2, 1, 2, 2), 
  Imb.end = c(2, 3, 3, 3, 1.5, 3, 3)
) -> names.t

fig3.data %>% 
  bind_rows(fig3.subs) %>% 
  left_join(names.t, by="Operation") %>% 

  ggplot()+
  geom_rect(
    data=. %>% filter(Operation %in%  c("em_primal", "error_estimate")), 
    aes(
      xmin=Start, xmax=End, 
      ymin=Imbrication, ymax=Imb.end, 
      fill=name
    ), color="black", size=0.3
  )+

  geom_rect(
    data=. %>% filter(Operation %in%  c("em_derivs")), 
    aes(
      xmin=Start, xmax=End, 
      ymin=Imbrication, ymax=Imb.end, 
      fill=name
    ), color="black", size=0.3, alpha=.7
  )+
  geom_rect(
    data=. %>% filter(Operation %in%  c("subset")), 
    aes(
      xmin=Start, xmax=End, 
      ymin=Imbrication, ymax=Imb.end, 
      fill=name
    ), color="black", size=0.3
  )+
  geom_rect(
    data=. %>% filter(Operation %in%  c("em2dkx")) %>% mutate(isubset=1:n()), 
    aes(
      xmin=Start, xmax=End, 
      ymin=Imbrication, ymax=Imb.end, 
      fill=name
    ), color="black", size=0.3
  )+
  geom_text(
    data = . %>% filter(Operation %in%  c("em2dkx")) %>% mutate(isubset=1:n()) %>% filter(isubset %in% c(1, 2, 3, 4, 5, 6, 11, 16, 21, 26)),
    aes(label=isubset, x=Start+(End-Start)/2, y=Imbrication+.25), size=8, color="black"
  ) +
  geom_rect(
    data=. %>% filter(Operation %in%  c("mpi_worker_run_subset")), 
    aes(
      xmin=Start, xmax=End, 
      ymin=Imbrication, ymax=Imb.end
    ), color="black", size=0.3, fill="white"
  )+
  geom_text(data=. %>% head(1), aes(label="Refinement Group", x=13, y=1.25), size=8) +
  scale_fill_manual(
    values=RColorBrewer::brewer.pal(5, "Set2"),  
    limits=c("subset", "local_refinement", "estimate_error", "em_derivs", "em_primal")
  ) +
  theme_bw(base_size=18)+
  labs(x="Exec. Time (s)", y="Stack\nlevel", fill="Operation:") +
  scale_y_continuous(breaks=c(1.25, 1.75, 2.5), labels=c(1, 2, 3)) -> micro.plot

a <- (micro.plot + coord_cartesian(xlim=c(4.5, 20.21))) + theme(legend.position="top") + facet_wrap(~"Jacobian phase - Refinement Group 2")
a.zoom <- (micro.plot + coord_cartesian(xlim=c(4, 7.5), ylim=c(1.5, 3))) + theme(legend.position="none") + facet_wrap(~"Zoom - First 5 subsets") 

a + 
theme(
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
    panel.grid = element_blank(),
    legend.position = "top",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0), 
    axis.title.x = element_blank(),
    legend.text=element_text(size=12.3)
  ) +
  guides(fill = guide_legend(nrow = 1, byrow = TRUE)) +
  a.zoom + 
  theme(
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    legend.spacing = unit(1, "mm"),
    panel.grid = element_blank(),
    legend.position = "none",
    legend.justification = "left",
    legend.box.spacing = unit(0, "pt"),
    legend.box.margin = margin(0, 0, 0, 0) 
  ) +
  plot_layout(heights = c(1.5, 1), ncol=1) -> p

ggsave(
  dpi = 100,
  filename="fig3.png",
  plot = p,
  units="px",
  path = "./figures/",
  width = 1000,
  height = 600
)
